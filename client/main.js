
import {Meteor} from 'meteor/meteor';
import {Vue} from 'meteor/akryum:vue';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' 


import App from '/imports/ui/App.vue';

Vue.use(Vuetify)

Meteor.startup(() => {
  new Vue(App).$mount('app');
});
