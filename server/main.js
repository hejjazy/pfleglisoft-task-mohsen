import { Meteor } from 'meteor/meteor';
const KPI = new Mongo.Collection('kpis');
const Company = new Mongo.Collection('companies');

Meteor.startup(() => {

  const count = Company.find();
  
  if (count === 0) {
    Company.insert({name:'company 1'});
    Company.insert({name:'company 2'});
    Company.insert({name:'company 3'});
      KPI.insert({
          companyName: 'company 1',
          winningRate:{
            total:92,
            newCustomers: 214,
            requests: 1427
          },
          fulfillment:{
            total:94,
            fulfilledOrders:98,
            orders:41
          }, 
          satisfaction:{
            total:98,
            rating:32
          }
      });
      KPI.insert({
        companyName: 'company 2',
        winningRate:{
          total:70,
          newCustomers: 95,
          requests: 1427
        },
        fulfillment:{
          total:75,
          fulfilledOrders:99,
          orders:41
        }, 
        satisfaction:{
          total:95,
          rating:74
        }
    });
    KPI.insert({
      companyName: 'company 3',
      winningRate:{
        total:64,
        newCustomers: 214,
        requests: 374
      },
      fulfillment:{
        total:84,
        fulfilledOrders:100,
        orders:74
      }, 
      satisfaction:{
        total:93,
        rating:47
      }
  });
  }
});

Meteor.methods({
  'getKPIs'(companyName ) {
    return KPI.find({"companyName":companyName}).fetch()[0];
  },
  'insertKPI'(data) {
    KPI.insert(data);
  },
  'updateKPI'(id,set){
    KPI.update({_id:id}, { $set: set });
  },
  'deleteKPI'(id){
    KPI.remove({_id:id})
  },
  'getCompanies'() {
    return Company.find({}).fetch();
  },
});

