import { Mongo } from 'meteor/mongo';

module.exports={
    find: function(query){
        KPI.find(query).fetch();
    },
    insert: function(data){
        KPI.insert(data,callback);
    },
    update: function(id,set){
        KPI.update({_id:id}, { $set: set },callback);

    },
    delete: function(id){
        KPI.remove({_id:id},callback)
    }
}

